package com.example.javawebservices;

import lombok.Value;

@Value
public class CreateAnimal {
    String name;
    String binominalName;
}
