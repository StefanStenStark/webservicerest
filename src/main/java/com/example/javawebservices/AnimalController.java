package com.example.javawebservices;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/animals")
public class AnimalController {

    @GetMapping
    public List<Animal>all(){
        return List.of(
                new Animal(UUID.randomUUID().toString(),"Zebra", "x",".","."),
                new Animal(UUID.randomUUID().toString(),"Elefant", "y", ".",".")

        );
    }
    @PostMapping
    public Animal createAnimal(@RequestBody CreateAnimal createAnimal){
        return new Animal(
                UUID.randomUUID().toString(),
                createAnimal.getName(),
                createAnimal.getBinominalName(),
                ".",
                ".");
    }
}
