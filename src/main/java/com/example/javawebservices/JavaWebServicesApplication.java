package com.example.javawebservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaWebServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaWebServicesApplication.class, args);
    }

}
