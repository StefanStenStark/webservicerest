package com.example.javawebservices;


import lombok.Value;

@Value
public class Animal {
    String id;
    String name;
    String binominalName;
    String description;
    String conservationStatus;
}
